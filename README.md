# Universifica Api

## Professor Fred
Olá! Para um resumo, assista: https://youtu.be/wFrT5oQwCyQ

[Pipelines](https://bitbucket.org/dnlcorrea31/universifica-api/addon/pipelines/home "Pipelines")

## Backend dos materiais de apoio e artigos 

1. Documentação da API (Yaml): [apidoc.yml](assets/apidoc.yml "Documentação da API")
2. Diagrama de Classes - universifica-api backend: [universifica-api.png](/assets/universifica-api.png "Diagrama de Classes - universifica-api backend")
3. Diagrama de Classes - universifica-pwa (Portal): [universifica-pwa.png](/assets/universifica-pwa.png "Diagrama de Classes - universifica-pwa (Portal).pdf")
4. PITCH UNIVERSIFICA: [PITCH UNIVERSIFICA.pdf](https://bitbucket.org/dnlcorrea31/universifica-api/raw/8a8e2282087635f4243c5fce840bc87bfd17cb9b/assets/PITCH%20UNIVERSIFICA.pdf "PITCH UNIVERSIFICA.pdf")
5. Diagrama de Entidades Plataforma: [universifica_DER.png](assets/universifica_DER.png "Universifica_DER.pdf")
6. Vídeo de start do server e testes: [testes.mp4](https://bitbucket.org/dnlcorrea31/universifica-api/raw/8a8e2282087635f4243c5fce840bc87bfd17cb9b/assets/testes.mp4 "testes.mp4")

## Olhar
- Rest-Assured.io - testes de integração
- Mock

## TODO
1. [x] ~~Nested Resource i.e. Categoria dentro de Artigo também vem com os links do HATEOAS~~
2. Entidades:
    1. [x] ~~Autor~~
    2. [x] ~~Imagem~~
    3. [x] ~~Media: [Spotify, SoundCloud, YouTube]~~
3. [ ] Testes
4. [ ] Todos os endpoints
    1. [x] /autores
    2. [x] /categorias
5. DTOs para os outros Resources
6. [ ] Slugs
7. Autenticação
    1. [x] ~~Proteger /autores com premium~~
    2. [ ] Proteger /autores/{id} com premium
    3. [ ] Resto das rotas...


## DevLog
- 2020/05/12 
    - DTOs criados para Artigo e Autor. A rota /artigos e /autores não acaba em loop infinito.

- 2020/05/07 
    - Autenticação básica ok. O que fazer para proteger /artigos/4, que é premium e só deve ser acessado com usuário logado? Não funfou...

- 2020/04/16
    - Imagem associada à Artigo.

- 2020/04/15
    - Resource da Categoria: Controller, Model, Assembler
    - Artigo associado à Categoria.
    - Artigo associado à Autor.


![Logo Universifica](assets/universifica.png "Universifica")
