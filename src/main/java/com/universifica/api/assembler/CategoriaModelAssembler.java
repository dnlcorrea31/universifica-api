package com.universifica.api.assembler;

import com.universifica.api.controllers.CategoriaController;
import com.universifica.api.model.Categoria;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class CategoriaModelAssembler implements RepresentationModelAssembler<Categoria, EntityModel<Categoria>> {

    @Override
    public EntityModel<Categoria> toModel(Categoria artigo) {
        return new EntityModel<>(artigo,
            linkTo(methodOn(CategoriaController.class).one(artigo.getId())).withSelfRel()
        );
    }

}
