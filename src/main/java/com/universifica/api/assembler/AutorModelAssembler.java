package com.universifica.api.assembler;

import com.universifica.api.controllers.AutorController;
import com.universifica.api.dto.AutorDTO;
import com.universifica.api.model.Autor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class AutorModelAssembler implements RepresentationModelAssembler<Autor, EntityModel<AutorDTO>> {

    @Override
    public EntityModel<AutorDTO> toModel(Autor autor) {

        AutorDTO autorDTO = new AutorDTO(autor);

        return new EntityModel<>(autorDTO,
            linkTo(methodOn(AutorController.class).one(autor.getId())).withSelfRel()
        );
    }

}
