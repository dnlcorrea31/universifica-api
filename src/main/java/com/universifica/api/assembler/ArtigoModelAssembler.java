package com.universifica.api.assembler;

import com.universifica.api.controllers.ArtigoController;
import com.universifica.api.dto.ArtigoDTO;
import com.universifica.api.model.Artigo;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class ArtigoModelAssembler implements RepresentationModelAssembler<Artigo, EntityModel<ArtigoDTO>> {

    @Override
    public EntityModel<ArtigoDTO> toModel(Artigo artigo) {

        ArtigoDTO artigoDTO = new ArtigoDTO(artigo);

        return new EntityModel<>(artigoDTO,
            linkTo(methodOn(ArtigoController.class).one(artigo.getId())).withSelfRel()
        );
    }

}
