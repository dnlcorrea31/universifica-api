package com.universifica.api.service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;
import java.util.stream.Collectors;

import javax.naming.directory.InvalidAttributesException;

import com.universifica.api.model.Autor;
import com.universifica.api.repository.AutorRepository;
import com.universifica.api.controllers.AutorController;
import com.universifica.api.dto.AutorDTO;
import com.universifica.api.assembler.AutorModelAssembler;
import com.universifica.api.exceptions.AutorNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;


@Service
public class AutorService {

    @Autowired
    private AutorRepository repository;

    @Autowired
    private AutorModelAssembler assembler;

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    public CollectionModel<EntityModel<AutorDTO>> all() {

        List<Autor> autores = repository.findAll();

        return new CollectionModel<>(autores.stream().map(assembler::toModel) .collect(Collectors.toList()),
                                     linkTo(methodOn(AutorController.class).all())
                                     .withSelfRel());
    }

    public EntityModel<AutorDTO> findById(Long id) {

        Autor autor = repository.findById(id).orElseThrow(() -> new AutorNotFoundException(id));

        return assembler.toModel(autor);
    }
}
