package com.universifica.api.service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;
import java.util.stream.Collectors;

import com.universifica.api.model.Artigo;
import com.universifica.api.repository.ArtigoRepository;
import com.universifica.api.controllers.ArtigoController;
import com.universifica.api.dto.ArtigoDTO;
import com.universifica.api.assembler.ArtigoModelAssembler;
import com.universifica.api.exceptions.ArtigoNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.hateoas.CollectionModel;


@Service
public class ArtigoService {

    private final ArtigoRepository repository;

    private final ArtigoModelAssembler assembler;

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    public ArtigoService(ArtigoRepository repository, ArtigoModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    public CollectionModel<EntityModel<ArtigoDTO>> all() {

        List<Artigo> artigos;

        if(auth != null) {
            artigos = repository.findByPremiumTrue();
        } else {
            artigos = repository.findByPremiumFalse();
        }

        return new CollectionModel<>(artigos.stream().map(assembler::toModel) .collect(Collectors.toList()),
                                     linkTo(methodOn(ArtigoController.class).all())
                                     .withSelfRel());
    }

    public EntityModel<ArtigoDTO> findById(Long id) {

        Artigo artigo = repository.findById(id)
            .orElseThrow(() -> new ArtigoNotFoundException(id));

        if(auth == null && artigo.getPremium() == true) {
            throw new AccessDeniedException("Nah nah, you gotta pay");
        }

        return assembler.toModel(artigo);
    }

    public CollectionModel<EntityModel<ArtigoDTO>> findBySlug(String slug) {
        List<Artigo> artigos;

        artigos = repository.findByCategorias_Slug(slug);

        return new CollectionModel<>(artigos.stream().map(assembler::toModel) .collect(Collectors.toList()),
                linkTo(methodOn(ArtigoController.class).all())
                        .withSelfRel());
    }
}
