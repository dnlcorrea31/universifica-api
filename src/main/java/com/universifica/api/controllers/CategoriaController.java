package com.universifica.api.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.CollectionModel;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.universifica.api.model.Categoria;
import com.universifica.api.repository.CategoriaRepository;
import com.universifica.api.assembler.CategoriaModelAssembler;
import com.universifica.api.exceptions.CategoriaNotFoundException;

@RestController
public class CategoriaController {

    private final CategoriaRepository repository;

    private final CategoriaModelAssembler assembler;

    public CategoriaController(CategoriaRepository repository, CategoriaModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/categorias")
    public CollectionModel<EntityModel<Categoria>> all() {

        List<EntityModel<Categoria>> categorias = repository.findAll().stream()
            .map(assembler::toModel)
            .collect(Collectors.toList());

        return new CollectionModel<>(categorias,
            linkTo(methodOn(CategoriaController.class).all()).withSelfRel());
    }

    // https://spring.io/guides/tutorials/rest/
    @GetMapping("/categorias/{id}")
    public EntityModel<Categoria> one(@PathVariable Long id) {

        Categoria artigo = repository.findById(id)
            .orElseThrow(() -> new CategoriaNotFoundException(id));

        return assembler.toModel(artigo);
    }
}
