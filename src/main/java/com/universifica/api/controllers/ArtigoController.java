package com.universifica.api.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.hateoas.EntityModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;

import com.universifica.api.service.ArtigoService;
import com.universifica.api.dto.ArtigoDTO;

@RestController
public class ArtigoController {

    @Autowired
    private ArtigoService service;

    @GetMapping("/artigos")
    public CollectionModel<EntityModel<ArtigoDTO>> all() {

        return service.all();

    }

    @GetMapping("/artigos/{id}")
    public EntityModel<ArtigoDTO> one(@PathVariable Long id) {

        return service.findById(id);

    }

    @GetMapping("/artigos/categoria/{slug}")
    public CollectionModel<EntityModel<ArtigoDTO>> findBySlug(@PathVariable String slug) {
        return service.findBySlug(slug);
    }
}
