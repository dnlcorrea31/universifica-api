package com.universifica.api.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.CollectionModel;

import com.universifica.api.service.AutorService;
import com.universifica.api.dto.AutorDTO;

@RestController
public class AutorController {

    private final AutorService service;

    public AutorController(AutorService service) {
        this.service = service;
    }

    @GetMapping("/autores")
    public CollectionModel<EntityModel<AutorDTO>> all() {

        return service.all();

    }

    @GetMapping("/autores/{id}")
    public EntityModel<AutorDTO> one(@PathVariable Long id) {

        return service.findById(id);

    }
}
