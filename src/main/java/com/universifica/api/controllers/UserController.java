package com.universifica.api.controllers;

import java.security.Principal;
// import java.util.List;
// import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// import org.springframework.hateoas.EntityModel;
// import org.springframework.hateoas.CollectionModel;
// import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

// import com.universifica.api.model.User;
// import com.universifica.api.repository.UserRepository;
// import com.universifica.api.assembler.UserModelAssembler;
// import com.universifica.api.exceptions.UserNotFoundException;

@RestController
public class UserController {

    // private final UserRepository repository;

    // private final UserModelAssembler assembler;

    // public UserController(UserRepository repository, UserModelAssembler assembler) {
    //     this.repository = repository;
    //     this.assembler = assembler;
    // }

    @GetMapping("/users")
    @ResponseBody
    // public Principal user (Principal user) {
    public String getUsers () {
        return "{\"users\":[{\"name\":\"Lucas\", \"country\":\"Brazil\"}," +
		           "{\"name\":\"Jackie\",\"country\":\"China\"}]}";
    }
}
