package com.universifica.api.dto;

import com.universifica.api.model.Autor;

import lombok.Data;

@Data
public class AutorDTO {

    private Long id;
    private String nome;
    private String email;
    private String minibio;
    private String slug;
    private String linkedin;
    private  String avatar;

    public AutorDTO(Autor autor) {
     this.nome = autor.getNome();
     this.email = autor.getEmail();
     this.minibio = autor.getMinibio();
     this.slug = autor.getSlug();
     this.linkedin = autor.getLinkedin();
     this.avatar = autor.getAvatar();
    }
}
