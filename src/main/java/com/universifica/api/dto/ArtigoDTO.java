package com.universifica.api.dto;

import com.universifica.api.model.Artigo;
import com.universifica.api.model.Categoria;
import com.universifica.api.model.Imagem;
import com.universifica.api.assembler.AutorModelAssembler;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class ArtigoDTO {

    @Autowired
    private AutorModelAssembler assembler;

    private Long id;
    private Boolean destaque;
    private String conteudo;
    private String titulo;
    private String descricao;
    private Boolean premium;
    private Imagem imagemDestaque;
    private LocalDateTime dataPublicacao;
    private String slug;

    private List<AutorDTO> autores;
    private List<Categoria> categorias;

    public ArtigoDTO(Artigo artigo) {
        this.destaque       = artigo.getDestaque();
        this.conteudo       = artigo.getConteudo();
        this.titulo         = artigo.getTitulo();
        this.descricao      = artigo.getDescricao();
        this.dataPublicacao = artigo.getDataPublicacao();
        this.slug           = artigo.getSlug();
        this.premium        = artigo.getPremium();
        this.imagemDestaque = artigo.getImagemDestaque();
        this.categorias     = artigo.getCategorias();

        this.autores = artigo.getAutores().stream()
            .map(a -> {
            return new AutorDTO(a);
        }).collect(Collectors.toList());
    }
}
