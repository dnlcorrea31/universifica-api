package com.universifica.api.exceptions;

public class AutorNotFoundException extends RuntimeException
{
    public AutorNotFoundException(Long id) {
        super("Autor não encontrado");
    }
}
