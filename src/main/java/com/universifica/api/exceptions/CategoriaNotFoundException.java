package com.universifica.api.exceptions;

public class CategoriaNotFoundException extends RuntimeException
{
    public CategoriaNotFoundException(Long id) {
        super("Categoria não encontrada " + id);
    }
}
