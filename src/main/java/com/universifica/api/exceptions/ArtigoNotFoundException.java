package com.universifica.api.exceptions;

public class ArtigoNotFoundException extends RuntimeException
{
    public ArtigoNotFoundException(Long id) {
        super("Artigo não encontrado " + id);
    }
}
