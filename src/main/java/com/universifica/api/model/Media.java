package com.universifica.api.model;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import lombok.Data;


// http://jpaobjects.sourceforge.net/m2-site/main/documentation/docbkx/html/user-guide/ch04s09.html

@Data
@Entity
@Table(name = "medias")
public class Media {

    enum Tipo {
        youtube,
        spotify,
        soundcloud
    }

    public static boolean contains(String test) {

        for (Tipo t : Tipo.values()) {
            if (t.name().equals(test)) {
                return true;
            }
        }

        return false;
    }

    private @Id @GeneratedValue Long id;

    public Media(){}

    private String identificador;

    private String tipo;

    /**
     * Constructor for Media
     * String identificador
     * String tipo
     */
	public Media(String identificador, String tipo) {
		this.identificador = identificador;

        if (!contains(tipo)) {
            throw new IllegalArgumentException("Tipo errado " + tipo);
        }
		this.tipo = tipo;
	}
}
