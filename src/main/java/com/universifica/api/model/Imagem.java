package com.universifica.api.model;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import lombok.Data;

@Data
@Entity
@Table(name = "imagens")
public class Imagem {

    public Imagem(){}

    private @Id @GeneratedValue Long id;

    private String url;

    private String mimetype;

    /**
     * Constructor for Imagem
     * String hash
     * String mimetype
     */
	public Imagem(String url, String mimetype) {
		this.url = url;
		this.mimetype = mimetype;
	}

}
