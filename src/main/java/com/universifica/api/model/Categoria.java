package com.universifica.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name = "categorias")
public class Categoria
{

    private @Id @GeneratedValue Long id;

    private String nome;

    // TODO: ...?
    private String slug;

	public Categoria() {}

	public Categoria(String nome, String slug) {
		this.nome = nome;
		this.slug = slug;
	}

    //TODO: artigos
    //private Artigos ....

}
