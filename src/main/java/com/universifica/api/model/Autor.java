package com.universifica.api.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "autores")
public class Autor {

    public Autor(){}

    private @Id @GeneratedValue Long id;

    private String nome;

    private String email;

    private String minibio;

    private String slug;

    private String linkedin;

    private  String avatar;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
               name = "artigos_autores",
               joinColumns = @JoinColumn(name = "autores_id"),
               inverseJoinColumns = @JoinColumn(name = "artigo_id"))
    // Lazy Load
    private List<Artigo> artigos;

	public Autor(String nome, String email, String minibio, String linkedin, String avatar) {
		this.nome = nome;
		this.email = email;
		this.minibio = minibio;
		this.linkedin = linkedin;
		this.avatar = avatar;
	}
}
