package com.universifica.api.model;

import java.util.List;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import java.time.LocalDateTime;

import lombok.Data;


@Data
@Entity
@Table(name = "artigos")
public class Artigo
{
    public Artigo() {}

    public Artigo(Boolean destaque, String conteudo, String titulo, String descricao, String slug, Boolean premium) {
        this.destaque = destaque;
        this.conteudo = conteudo;
        this.titulo = titulo;
        this.descricao = descricao;
        this.setDataPublicacao();
        this.slug = slug;
        this.premium = premium;
    }

    private @Id @GeneratedValue Long id;
    private Boolean destaque;


    @Column(name="conteudo", columnDefinition="TEXT")
    private String conteudo;
    private String titulo;
    private String descricao;

    @Column(columnDefinition = "boolean default false")
    private Boolean premium;


    private LocalDateTime dataPublicacao;

    // TODO: ...?
    private String slug;

    @ManyToMany
    private List<Categoria> categorias;

    @ManyToMany
    private List<Autor> autores;

    @OneToOne
    private Imagem imagemDestaque;

    @OneToMany
    private List<Media> medias;

    @PrePersist
    public void setDataPublicacao() {
        this.dataPublicacao = LocalDateTime.now();
    }

}
