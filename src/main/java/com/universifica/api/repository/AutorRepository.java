package com.universifica.api.repository;

import com.universifica.api.model.Autor;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AutorRepository extends JpaRepository<Autor,Long> {

}
