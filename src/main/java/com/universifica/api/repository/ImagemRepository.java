package com.universifica.api.repository;

import com.universifica.api.model.Imagem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImagemRepository extends JpaRepository<Imagem,Long> {}
