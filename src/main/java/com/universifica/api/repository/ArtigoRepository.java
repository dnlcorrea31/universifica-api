package com.universifica.api.repository;

import com.universifica.api.model.Artigo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArtigoRepository extends JpaRepository<Artigo,Long> {

    List<Artigo> findByPremiumTrue();

    List<Artigo> findByPremiumFalse();

    List<Artigo> findByCategorias_Slug(String slug);
}
