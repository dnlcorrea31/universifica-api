package com.universifica.api.factory;

import com.github.javafaker.Faker;
import com.github.slugify.Slugify;


public class Factory
{
    public static Faker faker = new Faker();

    public static Slugify slugger = new Slugify();
}
