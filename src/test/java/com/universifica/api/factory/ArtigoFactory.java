package com.universifica.api.factory;

import com.universifica.api.model.Artigo;

public class ArtigoFactory extends Factory
{
    public static Artigo create() {
        String titulo = faker.lorem().sentence();

        Artigo artigo = new Artigo(false, faker.lorem().paragraph(), titulo,
                                   faker.lorem().sentence(), slugger.slugify(titulo), false);



        return artigo;
    }

}
