package com.universifica.api.factory;

import com.universifica.api.model.Categoria;

public class CategoriaFactory extends Factory {

    public static Categoria create() {
        String cat = faker.lorem().word();

        return new Categoria(
            cat,
            slugger.slugify(cat)
        );
    }

}
