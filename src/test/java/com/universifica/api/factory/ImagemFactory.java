package com.universifica.api.factory;

import com.universifica.api.model.Imagem;

import com.github.javafaker.Faker;

public class ImagemFactory {

    public static Imagem create() {
        Faker f = new Faker();

        return new Imagem(f.file().fileName(), "image/jpeg");
    }

}
