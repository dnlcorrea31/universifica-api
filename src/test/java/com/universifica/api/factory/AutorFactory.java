package com.universifica.api.factory;

import com.universifica.api.model.Autor;

public class AutorFactory extends Factory
{
    public static Autor create() {
        return new Autor(
                         faker.harryPotter().character(),
                         faker.internet().emailAddress(),
                         faker.lorem().paragraph(),
                         "https://linkedin.com/foobar",
                         "https://www.gravatar.com/avatar/d9a016d18815f09defaa369bf3dfd461"
                         );
    }
}
