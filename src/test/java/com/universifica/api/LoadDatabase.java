package com.universifica.api;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;

import com.universifica.api.model.*;
import com.universifica.api.repository.*;
import com.universifica.api.factory.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase
{

    private com.github.javafaker.Faker faker;

    private List<Categoria> lista = new ArrayList<>();

    private List<Autor> autores = new ArrayList<>();

    @Bean
    public CommandLineRunner initDatabase(ArtigoRepository artigoRepository,
                                          CategoriaRepository categoriaRepository,
                                          AutorRepository autorRepository,
                                          ImagemRepository imagemRepository,
                                          MediaRepository mediaRepository)
    {

        this.faker = new Faker();

        Categoria categoria = new Categoria("projetos", "projetos");

        Autor autor = new Autor("Giuliano Ginani", "giuliano@universifica.com",
                                "<Preencher Minibio>", "linkedin",
                                "http://1.gravatar.com/avatar/d5fa8ac362cee221a068e9c036d555c5");

        categoriaRepository.save(categoria);
        autorRepository.save(autor);

        this.lista.add(categoria);
        this.autores.add(autor);

        return args -> {

            // List<Media> medias = new ArrayList<Media>();

            // for (int l = 0; l < 3; l++) {
            //     Media media = MediaFactory.create();
            //     medias.add(media);
            //     mediaRepository.save(media);
            // }

            Imagem imagem = new Imagem("54a9810c2d86bed2dfe683cc693a2f0c", "image/jpeg");
            imagemRepository.save(imagem);

            Artigo artigo = new Artigo(true, "Na vida universitária somos desafiados a realizar os mais diversos projetos para desenvolver competências preparando o caminho para assumir as profissões que desejamos. São pequenos ensaios sobre nossas profissões, você já pensou sobre isso?&/Quando pensamos em projetos universitários em geral podemos classificá-los em 3 categorias gerais, de acordo com suas características e exigências. Claro que em um determinado projeto que você esteja realizando é possível ter elementos dos 3 tipos, mas sempre predomina um em questão.&/Os três tipos de projetos são diferentes, em geral, pela sua natureza e objetivos centrais, abaixo apresentemos uma descrição breve de cada um deles:&/Extensão Universitária: São projetos de contato direto com a comunidade, muitas vezes prestando serviços. Clínicas escola, Capacitações, Eventos culturais são exemplos comuns e permitem desenvolver a capacidade de problematização, empatia e atendimento direto à população. Alguns exemplos seriam: um projeto para redução de violência nas escolas, organização de um cursinho pré-universitário comunitário ou mesmo palestras para prevenção de uma determinada doença.&/Iniciação Científica: São projetos que visam aproximar os alunos da aplicação do método científico tentando responder perguntas bem definidas e em geral fazem parte da linha de pesquisa de um professor orientador. Estas pesquisas podem ser feitas em ambiente controlado como laboratórios ou não, de acordo com a abordagem utilizada. Estas atividades permitem desenvolver a capacidade de desenvolver hipóteses e escolher métodos adequados a cada uma delas. Eis alguns exemplos: Identificação das propriedades terapêuticas de uma planta, Comparação entre diferentes técnicas utilizadas, Mapeamento das características de um determinado grupo.&/Inovação tecnológica: São projetos dedicados a criação de novos negócios e tecnologias, tem forte apelo na aplicabilidade da ciência no desenvolvimento de produtos. Estas pesquisas são direcionadas a solucionar um problema criando uma proposta de valor, com isso os estudantes têm a possibilidade de desenvolver a capacidade de modelagem de negócios e refletir sobre o uso prático de conceitos acadêmicos. Criação de dispositivos de automação para o agronegócio, Desenvolvimento de novas técnicas de diagnóstico em saúde, Elaboração de processos mais eficientes para o funcionamento de empresas são exemplos deste tipo de projeto.&/Cada um desses tipos de projetos permite desenvolver competências importantes para melhorar a capacidade de adaptação aos diferentes cenários de trabalho que tem surgido ao longo do tempo, vale saber disso para poder criar oportunidades durante sua formação.&/Você já realizou algum projeto no seu curso de graduação? Ele se encaixa em qual tipo de projeto? O que poderia aprender com cada um desses projetos?", "Você sabe qual o tipo do projeto que está fazendo?", "", "voce-sabe-qual-tipo-do-projeto-esta-fazendo", false);

            artigo.setCategorias(this.lista);
            artigo.setAutores(this.autores);
            artigo.setImagemDestaque(imagem);
            //artigo.setMedias(medias);


            log.info("Preloading Artigos " + artigoRepository.save(artigo));
        };
    }
}
